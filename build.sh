#!/bin/bash

build(){

    if [ "${CPU}" == "aarch64" ]; then
        CPUVARDIR=aarch64le
        CPUVAR=aarch64le
    elif [ "${CPU}" == "armv7" ]; then
        CPUVARDIR=armle-v7
        CPUVAR=armv7le
    elif [ "${CPU}" == "x86_64" ]; then
        CPUVARDIR=x86_64
        CPUVAR=x86_64
    else
        echo "Invalid architecture. Exiting..."
        exit 1
    fi

    echo "CPU set to ${CPUVAR}"
    echo "CPUVARDIR set to ${CPUVARDIR}"

    export CPUVARDIR CPUVAR
    export ARCH=${CPU}

    if [ -e CMakeCache.txt ]; then
      rm CMakeCache.txt
    fi
    mkdir -p build/${CPUVARDIR}
    cd build/${CPUVARDIR}
    cmake --log-level=TRACE \
        -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
        -DCMAKE_TOOLCHAIN_FILE=../../qnx.nto.toolchain.cmake \
        -DCMAKE_INSTALL_PREFIX=${QNX_STAGE} \
        -DCMAKE_BUILD_TYPE=Release \
        -DPYBIND11_TEST=OFF \
        ../.. || exit 1

    make -j$(nproc) || { echo "${PWD##*/}: make failed" ; exit 1; }
    make install  || { echo "${PWD##*/}: make install failed" ; exit 1; }
    cd ../../

}

echo "Building ${PWD##*/}..."

if [ ! -d "${QNX_TARGET}" ]; then
    echo "QNX_TARGET is not set. Exiting..."
    exit 1
fi

if [ ! -d "${QNX_STAGE}" ]; then
    echo "QNX_STAGE is not set. Exiting..."
    exit 1
fi

git clone https://github.com/pybind/pybind11.git /tmp/pybind11
cd /tmp/pybind11
git checkout edda89ba6851ba9c04052079f43f514aec37f9f2
cd -
rsync -haz /tmp/pybind11/* .
rm -rf /tmp/pybind11

rm -rf build/ install/

CPUS=("aarch64" "armv7" "x86_64")
if [ -z "$CPU" ]; then
    for CPU in ${CPUS[@]}; do
        build
    done
elif [ $CPU == "x86_64" ] || [ $CPU == "armv7" ] || [ $CPU == "aarch64" ] ; then
    build
else
    echo "invalid $CPU please set arch to one of the following x86_64, armv7, or aarch64 or unset arch to build all platforms"
    exit 1
fi